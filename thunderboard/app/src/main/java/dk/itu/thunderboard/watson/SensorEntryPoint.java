package dk.itu.thunderboard.watson;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.ibm.iotf.client.device.Command;
import com.ibm.iotf.client.device.CommandCallback;
import com.ibm.iotf.client.device.DeviceClient;
import com.silabs.thunderboard.ble.ThunderBoardSensor;
import com.silabs.thunderboard.common.data.PreferenceManager;
import com.silabs.thunderboard.common.data.model.ThunderBoardPreferences;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.Properties;
import java.util.concurrent.atomic.AtomicReference;

import dk.itu.thunderboard.watson.models.DeviceConfigurationMessage;
import dk.itu.thunderboard.watson.models.DeviceStatus;

/**
 * Created by Sebastian Dybdal Ehlers on 23/02/2017.
 */

public class SensorEntryPoint implements CommandCallback {
    private static final String DeviceAcknowledgeConfiguration = "config-reckon";
    private static final String DeviceConfigurationCommand = "config-device";

    private static SensorEntryPoint ourInstance;
    public synchronized static SensorEntryPoint getInstance() {
        if (ourInstance == null)
            ourInstance = new SensorEntryPoint();
        return ourInstance;
    }

    private boolean isActive = true;

    private AtomicReference<DeviceClient> client = new AtomicReference<>(null);

    private final ObjectMapper mapper;

    private SensorEntryPoint() {
        mapper = new ObjectMapper();
        mapper.registerModule(new JodaModule());

        ThunderBoardPreferences persistedPreferences = PreferenceManager.getInMemoryPreferences();
        client.set(getClient(
                persistedPreferences.getOrganizationId(),
                persistedPreferences.getDeviceType(),
                persistedPreferences.getVirtualId(),
                persistedPreferences.getAuthenticationToken()));
    }

    private DeviceClient getClient(String org, String type, String id, String token) {
        Properties options = new Properties();
        options.setProperty("org", org);
        options.setProperty("type", type);
        options.setProperty("id", id);
        options.setProperty("auth-token", token);
        options.setProperty("auth-method", "token");
        options.setProperty("clean-session", "true");
        try {
            DeviceClient _client = new DeviceClient(options);
            _client.setCommandCallback(this);
            _client.connect();
            return _client;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void push(ThunderBoardSensor sensor) {
        try {
            //TODO: Actually check isActive status
            String dataString = mapper.writeValueAsString(sensor.getSensorData());
            client.get().publishEvent("status", dataString, "json", 0);
        } catch (Exception e) {
            e.printStackTrace();
            //TODO: Probably handle the exception...
        }
    }

    @Override
    public void processCommand(Command cmd) {
        if (cmd.getCommand().equals(DeviceConfigurationCommand)) {
            //Get new configuration
            DeviceConfigurationMessage msg = null;
            try {
                String jsonMsg = new String(cmd.getRawPayload(), "UTF-8");
                msg = mapper.readValue(jsonMsg, DeviceConfigurationMessage.class);
            } catch (java.io.IOException e) {
                e.printStackTrace();
                return;
            }

            //Discard if invalid time is overdue
            DateTime now = DateTime.now(DateTimeZone.UTC);
            if (now.isAfter(msg.InvalidAfter))
                return;

            try {
                //Set new client and kill old client
                DeviceClient oldClient = client.getAndSet(getClient(msg.OrganizationId, msg.DeviceType, msg.VirtualId, msg.AuthenticationToken));
                isActive = msg.DeviceStatus == DeviceStatus.Active;

                //Kill old client
                killOldClient(oldClient);

                //Acknowledge at new Client
                client.get().publishEvent(
                        DeviceAcknowledgeConfiguration,
                        "",
                        "json",
                        2
                );
                //TODO: THE NEW CONFIGURATION IS NOT SAVED (Reverts back to settings from persistedPreferences)!
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void killOldClient(final DeviceClient _client) {
        //Close connection and set exception thrower (Should never happen)
        //HACK: Must close the connection from another thread otherwise the connection will not actually close
        Thread dcThread = new Thread(new Runnable(){
            public void run() {
                _client.disconnect();

                //Throw exception if actually called even when client is disconnected
                _client.setCommandCallback(new CommandCallback() {@Override public void processCommand(Command cmd) {
                    throw new RuntimeException("This should never happen....");
                }});
            }
        });
        dcThread.start();
    }
}