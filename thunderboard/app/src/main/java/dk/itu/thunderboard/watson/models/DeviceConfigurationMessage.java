package dk.itu.thunderboard.watson.models;

import org.joda.time.DateTime;

public class DeviceConfigurationMessage {
    public DateTime InvalidAfter;
    public String OrganizationId;
    public String AuthenticationToken;
    public String DeviceType;
    public String VirtualId;
    public DeviceStatus DeviceStatus;
}

