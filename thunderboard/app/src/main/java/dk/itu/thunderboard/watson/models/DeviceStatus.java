package dk.itu.thunderboard.watson.models;

public enum DeviceStatus
{
    Active(1),
    Inactive(0);

    private final int id;
    DeviceStatus(int id) { this.id = id; }
    public int getValue() { return id; }
}
